# Django app with Allauth configuration

This is a simple Django app with Allauth integrated

## Installing backend (works with python2.7)
```
git clone https://admirn@bitbucket.org/admirn/django-allauth.git
```
```
cd django-allauth
```
```
virtualenv env
```
```
source ./env/bin/activate
```
```
pip install -r requirements.txt
```
```
cd backend
```
```
python manage.py makemigrations
```
```
python manage.py migrate
```
```
python manage.py runserver
```
Needs SMTP configuration for sending emails. All configuration is listed in backend/settings.py
